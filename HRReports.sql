--task 1
select first_name, job_title, department_name
from employees e
ınner join jobs j on j.job_ıd = e.job_ıd
ınner join departments d on d.department_ıd = e.department_ıd;

--task 2
select distinct department_name, count (first_name) as total_number_of_employees from employees e
inner join departments d on e.department_id = d.department_id
group by department_name
order by department_name;

--task 3
select distinct job_title, avg(salary) as average_salary from employees e
inner join jobs j on j.job_id = e.job_id
group by job_title
order by job_title;

--task 4
select distinct department_name, sum(salary) as total_salary from employees e
inner join departments d on e.department_id = d.department_id
group by department_name
order by department_name;

--task 5
select first_name, salary, job_title from employees e
inner join jobs j on j.job_id = e.job_id
order by salary desc
fetch next 5 rows only;

--task 6
select extract (year from hire_date), count(first_name) as number_of_employees from employees
group by extract (year from hire_date)
order by extract (year from hire_date); 

--task 7
select distinct job_id, avg (salary) from employees
group by job_id;

--task 8
select employees.*, round (a.sal, 2) from employees
join (select avg(salary) as sal, department_id from employees
group by department_id) a on employees.department_id = a.department_id
where employees.salary > a.sal;

--task 9
select distinct department_name, count(first_name) as number_of_employees from employees e
inner join departments d on e.department_id = d.department_id
group by department_name
order by count(first_name) desc;

--task 10
select distinct department_name, min(salary), max(salary), avg(salary) from employees e
inner join departments d
on e.department_id = d.department_id
group by department_name;

--task 11
select employee_id,hire_date
from employees 
where extract(year from hire_date)<=extract(year from sysdate)-10;

--task 12
select distinct job_title, count(first_name) from employees e
inner join jobs j
on j.job_id = e.job_id
group by job_title;

--task 13
select e.manager_id,
m.first_name || ' ' ||m.last_name,
d.department_name,
sum(e.salary) as total_salary
from employees e
join departments d on e.department_id=d.department_id
join employees m on e.manager_id=d.manager_id
group by d.department_name,m.first_name,
m.last_name,e.manager_id;

--task 14
select * from employees e, employees m
where e.manager_id = m.employee_id and e.salary> m.salary;

--task 15
select distinct department_name, count(first_name)
from employees e
inner join departments d
on e.department_id = d.department_id
group by department_name
order by department_name;

--task 16
select avg(salary), j.job_title from employees e
inner join jobs j
on j.job_id = e.job_id
where job_title not in ('President')
group by j.job_title;

--task 17
select department_name, avg(salary) from employees e
inner join departments d
on d.department_id = e.department_id
group by department_name
order by avg(salary) desc
fetch next 5 rows only;


--task 18
select distinct department_name, sum (salary) as total_salary from employees e
inner join departments d on d.department_id = e.department_id
group by department_name
order by sum (salary) desc;

--task 19
select job_title, min(salary), max(salary), avg(salary) from employees e
inner join jobs j on j.job_id = e.job_id
group by job_title;

--task 20
select e1.employee_ıd, j.job_title, e1.manager_ıd from employees e1
ınner join employees e2
on e1.manager_ıd = e2.manager_ıd
and e1.job_ıd = e2.job_ıd
ınner join jobs j on e1.job_ıd = j.job_ıd
order by manager_ıd;
